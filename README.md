# MultiStageDisplay #

You will need to install [Processing 3](http://processing.org) and [ControlP5](http://www.sojamo.de/libraries/controlP5/) and place this project in your Processing sketches folder.

### Compatibility ###

* Tested with ProPresenter5
* Should be compatible with ProPresenter4 and ProPresenter6

### How do I get set up? ###

Edit config.json with password, hostname (or IP), and port for each ProPresenter host.  You will likely want to make changes to the source code to fit your use case.  This code is ugly, and barely works.

  It should go without saying, don't use this to automate your toaster, microwave, or popcorn maker.  It may seem like a good idea at first, then everything crashes in the middle of the night when someone forgot to shut it down, and your building catches fire.

 