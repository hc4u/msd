import java.text.SimpleDateFormat;
import java.util.Date;
import processing.net.*;
import controlP5.*;
import java.awt.Frame;
import java.awt.BorderLayout;

int timer;

private ControlP5 cp5;

Bang bangConnect;
Bang bangDisconnect;

public void ConnectButton() {
  int y;
  for(y = 0; y < hosts.length; y++) {
    if(hosts[y].cStageDisplay == null) {
      hosts[y].sXMLPacket = "<StageDisplayLogin>" + hosts[y].sPassword + "</StageDisplayLogin>\r\n";
    
      try {
        hosts[y].cStageDisplay = new Client(this, hosts[y].sHostname, hosts[y].iPort);
      } catch(Exception e) {
        hosts[y].cStageDisplay = null;
      }
  
      if(hosts[y].cStageDisplay != null) {
        if(hosts[y].cStageDisplay.active()) {
          hosts[y].cStageDisplay.write(hosts[y].sXMLPacket);
        } else {
          hosts[y].cStageDisplay = null;
        }
      }
      hosts[y].sXMLPacket = "";
    }
  }
}

public void DisconnectButton() {
  int y;
  for(y = 0; y < hosts.length; y++) {
    if(hosts[y].cStageDisplay != null) {
      hosts[y].cStageDisplay.stop();
      hosts[y].cStageDisplay = null;
    }
  }
}  

class StageDisplayFrame {
  String Name;
  boolean BlockText;
  boolean Visible;
  int Width, Height;
  int xAxis, yAxis;
  int fontSize;
}

class StageDisplayLayout {
  boolean showBorder;
  StageDisplayFrame Clock;
  StageDisplayFrame ElapsedTime;
  StageDisplayFrame Timer1;
  StageDisplayFrame Timer2;
  StageDisplayFrame Timer3;
  StageDisplayFrame VideoCounter;
  StageDisplayFrame CurrentSlide;
  StageDisplayFrame NextSlide;
  StageDisplayFrame CurrentSlideNotes;
  StageDisplayFrame NextSlideNotes;
  StageDisplayFrame Message;
  StageDisplayFrame ChordChart;
}

class host {
  String sHostname;
  String sPassword;
  int iPort;

  String sXMLPacket;

  String sClock;
  String sElapsedTime;
  String sTimer1;
  String sTimer2;
  String sTimer3;
  String sVideoCounter;
  String sCurrentSlide;
  String sNextSlide;
  String sCurrentSlideNotes;
  String sNextSlideNotes;
  String sMessage;
  String sChordChart;

  boolean bElapsedTime;
  boolean bTimer1;
  boolean bTimer2;
  boolean bTimer3;
  boolean bVideoCounter;

  Client cStageDisplay;
}


String sDefaultPassword = "password";
String sDefaultHostname = "127.0.0.1";
int iDefaultPort = 45678;

host hosts[];


XML xAuthToken;
XML xLayout;
XML xDisplay;
int iAuthToken = 0;
int iLayout = 0;


StageDisplayLayout Layout;

JSONObject config;

// Creates an example config if one is not found.
void newconfig() {
  config = new JSONObject();

  JSONArray json_hosts = new JSONArray();
  for(int y = 0; y < 2; y++) {
    JSONObject json_host = new JSONObject();
    json_host.setString("password", sDefaultPassword);
    json_host.setString("hostname", "");
    json_host.setInt("port", iDefaultPort);
    json_hosts.setJSONObject(y, json_host);
  }
  config.setJSONArray("hosts", json_hosts);
}

void loadconfig() {
  try {
    config = loadJSONObject("config.json");
  } catch (Exception e) {
    config = null;
  }
  if(config == null) {
    println( "No config, loading defaults...");
    newconfig();
  }

  JSONArray json_hosts = config.getJSONArray("hosts");
  
  hosts = new host[json_hosts.size()];
  for(int y = 0; y < json_hosts.size(); y++) {
    hosts[y] = new host();
    JSONObject json_host = json_hosts.getJSONObject(y);
    if(json_host != null) {
      hosts[y].iPort = json_host.getInt("port", iDefaultPort);
      hosts[y].sHostname = json_host.getString("hostname", sDefaultHostname);
      hosts[y].sPassword = json_host.getString("password", sDefaultPassword);
  
      hosts[y].sXMLPacket = "";
    }
  }
}


void setup() {
  // You will likely need to change these to match your screen size and arrangement.
  size( 1920, 1080 );
  if( frame != null) {
    surface.setResizable( true);
    frame.setLocation(-1920,1080);
  }

  textSize( 16);
  background(0);

  timer = millis();

  // Feel free to hide the UI once you are comfortable with the stage display
  cp5 = new ControlP5(this);
  cp5.addFrameRate().setInterval(10).setPosition(width - 24, height - 10);

  bangConnect = cp5.addBang("ConnectButton")
     .setPosition(1720, 0)
     .setSize(80, 16)
     .setLabel("Connect to Pro5")
     ;

  bangDisconnect = cp5.addBang("DisconnectButton")
     .setPosition(1820, 0)
     .setSize(80, 16)
     .setLabel("Disconnect")
     ;
  // End of UI Init.

  println( "Loading 'config.json'...");
  loadconfig();
  
  Layout = new StageDisplayLayout();
  Layout.Clock = new StageDisplayFrame();
  Layout.Clock.Name = "Clock";
  Layout.Clock.BlockText = false;
  Layout.ElapsedTime = new StageDisplayFrame();
  Layout.ElapsedTime.Name = "Elapsed Time";
  Layout.ElapsedTime.BlockText = false;
  Layout.Timer1 = new StageDisplayFrame();
  Layout.Timer1.Name = "Timer 1";
  Layout.Timer1.BlockText = false;
  Layout.Timer2 = new StageDisplayFrame();
  Layout.Timer2.Name = "Timer 2";
  Layout.Timer2.BlockText = false;
  Layout.Timer3 = new StageDisplayFrame();
  Layout.Timer3.Name = "Timer 3";
  Layout.Timer3.BlockText = false;
  Layout.VideoCounter = new StageDisplayFrame();
  Layout.VideoCounter.Name = "Video Counter";
  Layout.VideoCounter.BlockText = false;
  Layout.CurrentSlide = new StageDisplayFrame();
  Layout.CurrentSlide.Name = "Current Slide";
  Layout.CurrentSlide.BlockText = true;
  Layout.NextSlide = new StageDisplayFrame();
  Layout.NextSlide.Name = "Next Slide";
  Layout.NextSlide.BlockText = true;
  Layout.CurrentSlideNotes = new StageDisplayFrame();
  Layout.CurrentSlideNotes.Name = "Current Slide Notes";
  Layout.CurrentSlideNotes.BlockText = true;
  Layout.NextSlideNotes = new StageDisplayFrame();
  Layout.NextSlideNotes.Name = "Next Slide Notes";
  Layout.NextSlideNotes.BlockText = true;
  Layout.Message = new StageDisplayFrame();
  Layout.Message.Name = "Message";
  Layout.Message.BlockText = true;
  Layout.ChordChart = new StageDisplayFrame();
  Layout.ChordChart.Name = "Chord Chart";
  Layout.ChordChart.BlockText = true;

  // Automatically connect at startup  
  ConnectButton();
}

void exit() {
  int y;
  println( "Exiting ...");
  saveJSONObject(config, "config.json");
  for(y = 0; y < hosts.length; y++) {
    if(hosts[y].cStageDisplay != null) {
      hosts[y].cStageDisplay.stop();
      hosts[y].cStageDisplay = null;
    }
  }
  super.exit();
}

void draw() {
  int iStart, iEnding;
  int i, y, z;

  background(0);

  for(z = 0; z < hosts.length; z++) {
    if(hosts[z].cStageDisplay != null) {
      // Get new data from ProPresenter host
      if(hosts[z].cStageDisplay.available() > 0) {
        hosts[z].sXMLPacket += hosts[z].cStageDisplay.readString();
      }

      // Grab login reply packets    
      if((iStart = hosts[z].sXMLPacket.indexOf("<StageDisplayLogin")) == 0) {
        iAuthToken = 1;
        iEnding = hosts[z].sXMLPacket.indexOf(">");
        if(iEnding >= 0) {
          println("Got Login Status: " + hosts[z].sXMLPacket.substring(iStart, iEnding + 2));
          hosts[z].sXMLPacket = hosts[z].sXMLPacket.substring(iEnding + 2);
        }
      }

      // Parse Stage Display Layouts
      if((iStart = hosts[z].sXMLPacket.indexOf("<DisplayLayouts")) == 0) {
        iEnding = hosts[z].sXMLPacket.indexOf("</DisplayLayouts>");
        if(iEnding > 0) {
          //println(hosts[z].sXMLPacket.substring(0, iEnding + 17));
          xLayout = parseXML(hosts[z].sXMLPacket.substring(iStart, iEnding + 17));
          hosts[z].sXMLPacket = hosts[z].sXMLPacket.substring(iEnding + 17);
          // For now we only process the first host's layout.
          if(z == 0) {
            String sLayout = xLayout.getString("selected"); 
            XML[] layout = xLayout.getChildren("DisplayLayout");
            for (y = 0; y < layout.length; y++) {
              if(layout[y].getString("identifier").equals(sLayout)) {
                Layout.showBorder = (int(layout[y].getFloat("showBorder")) == 1);
                XML[] frames = layout[y].getChildren("Frame");
                for (i = 0; i < frames.length; i++) {
                  if(frames[i].getString("identifier").equals("Clock")) {
                    getFrameLayout(Layout.Clock, frames[i]);
                  } else
                  if(frames[i].getString("identifier").equals("ElapsedTime")) {
                    getFrameLayout(Layout.ElapsedTime, frames[i]);
                  } else
                  if(frames[i].getString("identifier").equals("Timer1")) {
                    getFrameLayout(Layout.Timer1, frames[i]);
                  } else
                  if(frames[i].getString("identifier").equals("Timer2")) {
                    getFrameLayout(Layout.Timer2, frames[i]);
                  } else
                  if(frames[i].getString("identifier").equals("Timer3")) {
                    getFrameLayout(Layout.Timer3, frames[i]);
                  } else
                  if(frames[i].getString("identifier").equals("VideoCounter")) {
                    getFrameLayout(Layout.VideoCounter, frames[i]);
                  } else
                  if(frames[i].getString("identifier").equals("CurrentSlide")) {
                    getFrameLayout(Layout.CurrentSlide, frames[i]);
                  } else
                  if(frames[i].getString("identifier").equals("NextSlide")) {
                    getFrameLayout(Layout.NextSlide, frames[i]);
                  } else
                  if(frames[i].getString("identifier").equals("CurrentSlideNotes")) {
                    getFrameLayout(Layout.CurrentSlideNotes, frames[i]);
                  } else
                  if(frames[i].getString("identifier").equals("NextSlideNotes")) {
                    getFrameLayout(Layout.NextSlideNotes, frames[i]);
                  } else
                  if(frames[i].getString("identifier").equals("Message")) {
                    getFrameLayout(Layout.Message, frames[i]);
                  } else
                  if(frames[i].getString("identifier").equals("ChordChart")) {
                    getFrameLayout(Layout.ChordChart, frames[i]);
                  }
                }
              }
            }
          }
        }
      }
      // Parse updates from the ProPresenter host.
      if((iStart = hosts[z].sXMLPacket.indexOf("<StageDisplayData")) == 0) {
        iEnding = hosts[z].sXMLPacket.indexOf("</StageDisplayData>");
        if(iEnding > 0) {
          hosts[z].sClock = "";
          hosts[z].sElapsedTime = "";
          hosts[z].sTimer1 = "";
          hosts[z].sTimer2 = "";
          hosts[z].sTimer3 = "";
          hosts[z].sVideoCounter = "";
          hosts[z].sCurrentSlide = "";
          hosts[z].sNextSlide = "";
          hosts[z].sCurrentSlideNotes = "";
          hosts[z].sNextSlideNotes = "";
          hosts[z].sMessage = "";
          hosts[z].sChordChart = "";
          //println(hosts[z].sXMLPacket.substring(0, iEnding + 19));
          xDisplay = parseXML(hosts[z].sXMLPacket.substring(iStart, iEnding + 19));
          hosts[z].sXMLPacket = hosts[z].sXMLPacket.substring(iEnding + 19); //<>//
          XML[] fields = xDisplay.getChildren("Fields/Field");
          for (i = 0; i < fields.length; i++) {
            if(fields[i].getString("identifier").equals("Clock")) {
              hosts[z].sClock = fields[i].getContent();
            } else
            if(fields[i].getString("identifier").equals("ElapsedTime")) {
              hosts[z].sElapsedTime = fields[i].getContent();
              hosts[z].bElapsedTime = fields[i].getString("running").equals("1");
            } else
            if(fields[i].getString("identifier").equals("Timer1")) {
              hosts[z].sTimer1 = fields[i].getContent();
              hosts[z].bTimer1 = fields[i].getString("running").equals("1");
            } else
            if(fields[i].getString("identifier").equals("Timer2")) {
              hosts[z].sTimer2 = fields[i].getContent();
              hosts[z].bTimer2 = fields[i].getString("running").equals("1");
            } else
            if(fields[i].getString("identifier").equals("Timer3")) {
              hosts[z].sTimer3 = fields[i].getContent();
              hosts[z].bTimer3 = fields[i].getString("running").equals("1");
            } else
            if(fields[i].getString("identifier").equals("VideoCounter")) {
              hosts[z].sVideoCounter = fields[i].getContent();
              hosts[z].bVideoCounter = fields[i].getString("running").equals("1");
            } else
            if(fields[i].getString("identifier").equals("CurrentSlide")) {
              hosts[z].sCurrentSlide = fields[i].getContent();
            } else
            if(fields[i].getString("identifier").equals("NextSlide")) {
              hosts[z].sNextSlide = fields[i].getContent();
            } else
            if(fields[i].getString("identifier").equals("CurrentSlideNotes")) {
              hosts[z].sCurrentSlideNotes = fields[i].getContent();
            } else
            if(fields[i].getString("identifier").equals("NextSlideNotes")) {
              hosts[z].sNextSlideNotes = fields[i].getContent();
            } else
            if(fields[i].getString("identifier").equals("Message")) {
              hosts[z].sMessage = fields[i].getContent();
            } else
            if(fields[i].getString("identifier").equals("ChordChart")) {
              hosts[z].sChordChart = fields[i].getContent();
            }
          }
  
        }
      }
      if((iStart = hosts[z].sXMLPacket.indexOf("<?")) == 0) {
        iAuthToken = 1;
        iEnding = hosts[z].sXMLPacket.indexOf("?>");
        if(iEnding >= 0) {
          //println(hosts[z].sXMLPacket.substring(0, iEnding + 2));
          hosts[z].sXMLPacket = hosts[z].sXMLPacket.substring(iEnding + 2);
        }
      }
      if((iStart = hosts[z].sXMLPacket.indexOf("\r")) == 0) {
          hosts[z].sXMLPacket = hosts[z].sXMLPacket.substring(iStart + 1);
      }
      if((iStart = hosts[z].sXMLPacket.indexOf("\n")) == 0) {
          hosts[z].sXMLPacket = hosts[z].sXMLPacket.substring(iStart + 1);
      }
    }
  }
  
  // The clock frame is always generated on the display machine,
  // not the ProPresenter host.
  Date d = new Date();
  String sClock = new java.text.SimpleDateFormat("hh:mm aa").format(d);
  drawFrame(Layout.Clock, sClock);

  // These timers are fed from the primary host. You can change this manually,
  // or use code similar to the VideoCounter frame to combine all the hosts.
  drawFrame(Layout.ElapsedTime, hosts[0].sElapsedTime);
  drawFrame(Layout.Timer1, hosts[0].sTimer1);
  drawFrame(Layout.Timer2, hosts[0].sTimer2);
  drawFrame(Layout.Timer3, hosts[0].sTimer3);
  drawFrame(Layout.CurrentSlideNotes, hosts[0].sCurrentSlideNotes);
  drawFrame(Layout.NextSlideNotes, hosts[0].sNextSlideNotes);
  drawFrame(Layout.Message, hosts[0].sMessage);

  String sCurrentSlide = "";
  String sNextSlide = "";

  // This shows the current slide / next slide frames from all the hosts combined.
  // You will likely want to make adjustments to suit your usage.
  for(z = 0; z < hosts.length; z++) {
    if((hosts[z].sCurrentSlide != null) && (hosts[z].sCurrentSlide.length() > 0)) {
      sCurrentSlide += hosts[z].sCurrentSlide + "\n";
    }
    if((hosts[z].sNextSlide != null)  && (hosts[z].sNextSlide.length() > 0)) {
      sNextSlide += hosts[z].sNextSlide + "\n";
    }
  }
  drawFrame(Layout.CurrentSlide, sCurrentSlide);
  drawFrame(Layout.NextSlide, sNextSlide);

  // Combines all active Video Countdown timers into one frame
  String sVideoCounter = "";
  for(z = 0; z < hosts.length; z++) {
    if(hosts[z].sVideoCounter != null && (hosts[z].sVideoCounter.indexOf("-") < 0)) {
      sVideoCounter += hosts[z].sVideoCounter + "\n";
    }
  }
  drawFrame(Layout.VideoCounter, sVideoCounter);

  // We don't use ChordCharts, so you will likely need to change this if you do.
  drawFrame(Layout.ChordChart, hosts[0].sChordChart);
  
  timerTick();
}

void drawFrame(StageDisplayFrame frameLayout, String frameText) {
  int y = frameLayout.yAxis;
  int h = frameLayout.Height;

  if(frameLayout.Visible) {
    textAlign(CENTER, CENTER);
    stroke(#ffffff);
    noFill();
    rect(frameLayout.xAxis, frameLayout.yAxis, frameLayout.Width, frameLayout.Height);
    noStroke();
    if(Layout.showBorder) {
      fill(#ffffff);
      rect(frameLayout.xAxis, frameLayout.yAxis, frameLayout.Width, 16);
      fill(#000000);
      textSize(12);
      text(frameLayout.Name, frameLayout.xAxis, frameLayout.yAxis, frameLayout.Width, 16);
      y += 16;
      h -= 16;
    }
    if(frameLayout.BlockText) {
      textAlign(CENTER, TOP);
    }
    if(frameLayout.fontSize > 0) {
      textSize(frameLayout.fontSize * 1.25);
    } else {
      textSize(80);
    }
    fill(#ffffff);
    if(frameText != null) {
      text(frameText, frameLayout.xAxis, y, frameLayout.Width, h);
    }
  }
}

void getFrameLayout(StageDisplayFrame frameLayout, XML frameConfig) {
  frameLayout.Visible = frameConfig.getString("isVisible").equals("YES");
  frameLayout.xAxis = int(frameConfig.getFloat("xAxis"));
  frameLayout.yAxis = int(frameConfig.getFloat("yAxis"));
  frameLayout.Width = int(frameConfig.getFloat("width"));
  frameLayout.Height = int(frameConfig.getFloat("height"));
  frameLayout.fontSize = int(frameConfig.getFloat("fontSize"));
}

void timerTick() {
  int z;
  if(millis() - timer < 1000) return;
  timer = millis();

  for(z = 0; z < hosts.length; z++) {
    if(hosts[z].bElapsedTime && (hosts[z].sElapsedTime.indexOf("-") < 0)) {
      String sTimer[] = split(hosts[z].sElapsedTime, ":");
      int iTimer = 0;
      iTimer = int(sTimer[0]) * 3600;
      iTimer += int(sTimer[1]) * 60;
      iTimer += int(sTimer[2]);
      iTimer ++;
      if(iTimer > 24 * 3600) {
        hosts[z].bElapsedTime = false;
      }
      hosts[z].sElapsedTime = String.format("%02d:%02d:%02d", (iTimer / 3600), (iTimer % 3600) / 60, (iTimer % 60));
    }
  
    if(hosts[z].bTimer1 && (hosts[z].sTimer1.indexOf("-") < 0)) {
      String sTimer[] = split(hosts[z].sTimer1, ":");
      int iTimer = 0;
      iTimer = int(sTimer[0]) * 60 * 60;
      iTimer += int(sTimer[1]) * 60;
      iTimer += int(sTimer[2]);
      iTimer --;
      if(iTimer <= 0) {
        iTimer = 0;
        hosts[z].bTimer1 = false;
      }
      hosts[z].sTimer1 = String.format("%02d:%02d:%02d", (iTimer / 3600), (iTimer % 3600) / 60, (iTimer % 60));
    }
  
    if(hosts[z].bTimer2 && (hosts[z].sTimer2.indexOf("-") < 0)) {
      String sTimer[] = split(hosts[z].sTimer2, ":");
      int iTimer = 0;
      iTimer = int(sTimer[0]) * 60 * 60;
      iTimer += int(sTimer[1]) * 60;
      iTimer += int(sTimer[2]);
      iTimer --;
      if(iTimer <= 0) {
        iTimer = 0;
        hosts[z].bTimer2 = false;
      }
      hosts[z].sTimer2 = String.format("%02d:%02d:%02d", (iTimer / 3600), (iTimer % 3600) / 60, (iTimer % 60));
    }
  
    if(hosts[z].bTimer3 && (hosts[z].sTimer3.indexOf("-") < 0)) {
      String sTimer[] = split(hosts[z].sTimer3, ":");
      int iTimer = 0;
      iTimer = int(sTimer[0]) * 60 * 60;
      iTimer += int(sTimer[1]) * 60;
      iTimer += int(sTimer[2]);
      iTimer --;
      if(iTimer <= 0) {
        iTimer = 0;
        hosts[z].bTimer3 = false;
      }
      hosts[z].sTimer3 = String.format("%02d:%02d:%02d", (iTimer / 3600), (iTimer % 3600) / 60, (iTimer % 60));
    }
  
    if(hosts[z].bVideoCounter && (hosts[z].sVideoCounter.indexOf("-") < 0)) {
      String sTimer[] = split(hosts[z].sVideoCounter, ":");
      int iTimer = 0;
      iTimer = int(sTimer[0]) * 60 * 60;
      iTimer += int(sTimer[1]) * 60;
      iTimer += int(sTimer[2]);
      iTimer --;
      if(iTimer <= 0) {
        iTimer = 0;
        hosts[z].bVideoCounter = false;
      }
      hosts[z].sVideoCounter = String.format("%02d:%02d:%02d", (iTimer / 3600), (iTimer % 3600) / 60, (iTimer % 60));
    }
  }
}
